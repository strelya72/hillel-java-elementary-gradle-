import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.SortedBidiMap;
import org.apache.commons.collections4.bidimap.DualTreeBidiMap;
import org.apache.commons.collections4.bidimap.TreeBidiMap;

import java.util.ArrayList;
import java.util.Collection;

public class Main {
    public static void main(String[] args) {

        // Task 2
        System.out.println("\tTask 2:");
        BidiMap<String, Integer> map = new TreeBidiMap<>();
        SortedBidiMap<String, Integer> map2 = new DualTreeBidiMap<>();

        map.put("SIX", 6);
        int intValue = map.get("SIX");
        String anotherValue = map.getKey(6);

        System.out.println(intValue);
        System.out.println(anotherValue);

        map.put("NewSix", 6);
        anotherValue = map.getKey(6);
        System.out.println(anotherValue);

        // Task 3
        System.out.println("\tTask 3:");
        CallLog callLog = new CallLog("+380997186857", "+380508090156",
                90, System.currentTimeMillis());

        Gson gson = new Gson();
        String json = gson.toJson(callLog);
        System.out.println(json);

        callLog = gson.fromJson(json, CallLog.class);

        // Task 5, 6
        System.out.println("\tTask 5, 6:");
        Collection<CallLog> listCallLogs = new ArrayList<CallLog>();
        int count = Integer.parseInt(args[0]);
        for (int i = 0; i < count; i++) {
            listCallLogs.add(newCallLog());
        }

        for (CallLog item : listCallLogs) {
            System.out.println(item.getiSenderPhoneNumber() + " - " + item.getiRecipientPhoneNummer());
        }


        // Task 7
        System.out.println("\tTask 7:");
        String json2 = gson.toJson(listCallLogs);
        System.out.println(json2);
        listCallLogs = gson.fromJson(json2, new TypeToken<Collection<CallLog>>(){}.getType());

        for (CallLog item : listCallLogs) {
            System.out.println(item.getiSenderPhoneNumber() + " - " + item.getiRecipientPhoneNummer());
        }
    }

    public static CallLog newCallLog() {
        String numberPhoneSender = "+" + (380000000000L + (long) (Math.random() * 999999999));
        String numberPhoneRecipient = "+" + (380000000000L + (long) (Math.random() * 999999999));
        int callDuration = (int) (30 + Math.random() * 150);

        return new CallLog(numberPhoneSender, numberPhoneRecipient, callDuration, System.currentTimeMillis());

    }
}
